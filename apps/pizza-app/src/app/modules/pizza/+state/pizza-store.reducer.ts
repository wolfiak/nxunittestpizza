import { PizzaStoreAction, PizzaStoreActionTypes } from './pizza-store.actions';

export const PIZZASTORE_FEATURE_KEY = 'pizzaStore';

/**
 * Interface for the 'PizzaStore' data used in
 *  - PizzaStoreState, and
 *  - pizzaStoreReducer
 *
 *  Note: replace if already defined in another module
 */

/* tslint:disable:no-empty-interface */
export interface Entity {}

export interface PizzaStoreState {
  list: Entity[]; // list of PizzaStore; analogous to a sql normalized table
  selectedId?: string | number; // which PizzaStore record has been selected
  loaded: boolean; // has the PizzaStore list been loaded
  error?: any; // last none error (if any)
}

export interface PizzaStorePartialState {
  readonly [PIZZASTORE_FEATURE_KEY]: PizzaStoreState;
}

export const initialState: PizzaStoreState = {
  list: [],
  loaded: false
};

export function pizzaStoreReducer(
  state: PizzaStoreState = initialState,
  action: PizzaStoreAction
): PizzaStoreState {
  switch (action.type) {
    case PizzaStoreActionTypes.PizzaStoreLoaded: {
      state = {
        ...state,
        list: action.payload,
        loaded: true
      };
      break;
    }
  }
  return state;
}
