import { Action } from '@ngrx/store';
import { Entity } from './pizza-store.reducer';

export enum PizzaStoreActionTypes {
  LoadPizzaStore = '[PizzaStore] Load PizzaStore',
  PizzaStoreLoaded = '[PizzaStore] PizzaStore Loaded',
  PizzaStoreLoadError = '[PizzaStore] PizzaStore Load Error'
}

export class LoadPizzaStore implements Action {
  readonly type = PizzaStoreActionTypes.LoadPizzaStore;
}

export class PizzaStoreLoadError implements Action {
  readonly type = PizzaStoreActionTypes.PizzaStoreLoadError;
  constructor(public payload: any) {}
}

export class PizzaStoreLoaded implements Action {
  readonly type = PizzaStoreActionTypes.PizzaStoreLoaded;
  constructor(public payload: Entity[]) {}
}

export type PizzaStoreAction =
  | LoadPizzaStore
  | PizzaStoreLoaded
  | PizzaStoreLoadError;

export const fromPizzaStoreActions = {
  LoadPizzaStore,
  PizzaStoreLoaded,
  PizzaStoreLoadError
};
