import { PizzaStoreLoaded } from './pizza-store.actions';
import {
  PizzaStoreState,
  Entity,
  initialState,
  pizzaStoreReducer
} from './pizza-store.reducer';

describe('PizzaStore Reducer', () => {
  const getPizzaStoreId = it => it['id'];
  let createPizzaStore;

  beforeEach(() => {
    createPizzaStore = (id: string, name = ''): Entity => ({
      id,
      name: name || `name-${id}`
    });
  });

  describe('valid PizzaStore actions ', () => {
    it('should return set the list of known PizzaStore', () => {
      const pizzaStores = [
        createPizzaStore('PRODUCT-AAA'),
        createPizzaStore('PRODUCT-zzz')
      ];
      const action = new PizzaStoreLoaded(pizzaStores);
      const result: PizzaStoreState = pizzaStoreReducer(initialState, action);
      const selId: string = getPizzaStoreId(result.list[1]);

      expect(result.loaded).toBe(true);
      expect(result.list.length).toBe(2);
      expect(selId).toBe('PRODUCT-zzz');
    });
  });

  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;
      const result = pizzaStoreReducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
