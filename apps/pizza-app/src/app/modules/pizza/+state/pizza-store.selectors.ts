import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PizzaStoreState } from './pizza-store.reducer';

// Lookup the 'PizzaStore' feature state managed by NgRx
const getPizzaStoreState = createFeatureSelector<PizzaStoreState>('pizzaStore');

const getLoaded = createSelector(
  getPizzaStoreState,
  (state: PizzaStoreState) => state.loaded
);
const getError = createSelector(
  getPizzaStoreState,
  (state: PizzaStoreState) => state.error
);

const getAllPizzaStore = createSelector(
  getPizzaStoreState,
  getLoaded,
  (state: PizzaStoreState, isLoaded) => {
    return isLoaded ? state.list : [];
  }
);
const getSelectedId = createSelector(
  getPizzaStoreState,
  (state: PizzaStoreState) => state.selectedId
);
const getSelectedPizzaStore = createSelector(
  getAllPizzaStore,
  getSelectedId,
  (pizzaStore, id) => {
    const result = pizzaStore.find(it => it['id'] === id);
    return result ? Object.assign({}, result) : undefined;
  }
);

export const pizzaStoreQuery = {
  getLoaded,
  getError,
  getAllPizzaStore,
  getSelectedPizzaStore
};
