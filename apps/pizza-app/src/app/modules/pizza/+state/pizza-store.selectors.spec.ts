import { Entity, PizzaStoreState } from './pizza-store.reducer';
import { pizzaStoreQuery } from './pizza-store.selectors';

describe('PizzaStore Selectors', () => {
  const ERROR_MSG = 'No Error Available';
  const getPizzaStoreId = it => it['id'];

  let storeState;

  beforeEach(() => {
    const createPizzaStore = (id: string, name = ''): Entity => ({
      id,
      name: name || `name-${id}`
    });
    storeState = {
      pizzaStore: {
        list: [
          createPizzaStore('PRODUCT-AAA'),
          createPizzaStore('PRODUCT-BBB'),
          createPizzaStore('PRODUCT-CCC')
        ],
        selectedId: 'PRODUCT-BBB',
        error: ERROR_MSG,
        loaded: true
      }
    };
  });

  describe('PizzaStore Selectors', () => {
    it('getAllPizzaStore() should return the list of PizzaStore', () => {
      const results = pizzaStoreQuery.getAllPizzaStore(storeState);
      const selId = getPizzaStoreId(results[1]);

      expect(results.length).toBe(3);
      expect(selId).toBe('PRODUCT-BBB');
    });

    it('getSelectedPizzaStore() should return the selected Entity', () => {
      const result = pizzaStoreQuery.getSelectedPizzaStore(storeState);
      const selId = getPizzaStoreId(result);

      expect(selId).toBe('PRODUCT-BBB');
    });

    it("getLoaded() should return the current 'loaded' status", () => {
      const result = pizzaStoreQuery.getLoaded(storeState);

      expect(result).toBe(true);
    });

    it("getError() should return the current 'error' storeState", () => {
      const result = pizzaStoreQuery.getError(storeState);

      expect(result).toBe(ERROR_MSG);
    });
  });
});
