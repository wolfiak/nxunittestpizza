import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/nx';

import { PizzaStorePartialState } from './pizza-store.reducer';
import {
  LoadPizzaStore,
  PizzaStoreLoaded,
  PizzaStoreLoadError,
  PizzaStoreActionTypes
} from './pizza-store.actions';

@Injectable()
export class PizzaStoreEffects {
  @Effect()
  loadPizzaStore$ = this.dataPersistence.fetch(
    PizzaStoreActionTypes.LoadPizzaStore,
    {
      run: (action: LoadPizzaStore, state: PizzaStorePartialState) => {
        // Your custom REST 'load' logic goes here. For now just return an empty list...
        return new PizzaStoreLoaded([]);
      },

      onError: (action: LoadPizzaStore, error) => {
        console.error('Error', error);
        return new PizzaStoreLoadError(error);
      }
    }
  );

  constructor(
    private actions$: Actions,
    private dataPersistence: DataPersistence<PizzaStorePartialState>
  ) {}
}
