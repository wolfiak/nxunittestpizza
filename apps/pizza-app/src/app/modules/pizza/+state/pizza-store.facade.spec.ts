import { NgModule } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { readFirst } from '@nrwl/nx/testing';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule, Store } from '@ngrx/store';

import { NxModule } from '@nrwl/nx';

import { PizzaStoreEffects } from './pizza-store.effects';
import { PizzaStoreFacade } from './pizza-store.facade';

import { pizzaStoreQuery } from './pizza-store.selectors';
import { LoadPizzaStore, PizzaStoreLoaded } from './pizza-store.actions';
import {
  PizzaStoreState,
  Entity,
  initialState,
  pizzaStoreReducer
} from './pizza-store.reducer';

interface TestSchema {
  pizzaStore: PizzaStoreState;
}

describe('PizzaStoreFacade', () => {
  let facade: PizzaStoreFacade;
  let store: Store<TestSchema>;
  let createPizzaStore;

  beforeEach(() => {
    createPizzaStore = (id: string, name = ''): Entity => ({
      id,
      name: name || `name-${id}`
    });
  });

  describe('used in NgModule', () => {
    beforeEach(() => {
      @NgModule({
        imports: [
          StoreModule.forFeature('pizzaStore', pizzaStoreReducer, {
            initialState
          }),
          EffectsModule.forFeature([PizzaStoreEffects])
        ],
        providers: [PizzaStoreFacade]
      })
      class CustomFeatureModule {}

      @NgModule({
        imports: [
          NxModule.forRoot(),
          StoreModule.forRoot({}),
          EffectsModule.forRoot([]),
          CustomFeatureModule
        ]
      })
      class RootModule {}
      TestBed.configureTestingModule({ imports: [RootModule] });

      store = TestBed.get(Store);
      facade = TestBed.get(PizzaStoreFacade);
    });

    /**
     * The initially generated facade::loadAll() returns empty array
     */
    it('loadAll() should return empty list with loaded == true', async done => {
      try {
        let list = await readFirst(facade.allPizzaStore$);
        let isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(0);
        expect(isLoaded).toBe(false);

        facade.loadAll();

        list = await readFirst(facade.allPizzaStore$);
        isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(0);
        expect(isLoaded).toBe(true);

        done();
      } catch (err) {
        done.fail(err);
      }
    });

    /**
     * Use `PizzaStoreLoaded` to manually submit list for state management
     */
    it('allPizzaStore$ should return the loaded list; and loaded flag == true', async done => {
      try {
        let list = await readFirst(facade.allPizzaStore$);
        let isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(0);
        expect(isLoaded).toBe(false);

        store.dispatch(
          new PizzaStoreLoaded([
            createPizzaStore('AAA'),
            createPizzaStore('BBB')
          ])
        );

        list = await readFirst(facade.allPizzaStore$);
        isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(2);
        expect(isLoaded).toBe(true);

        done();
      } catch (err) {
        done.fail(err);
      }
    });
  });
});
