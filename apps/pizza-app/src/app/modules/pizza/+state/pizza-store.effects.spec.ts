import { TestBed, async } from '@angular/core/testing';

import { Observable } from 'rxjs';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { provideMockActions } from '@ngrx/effects/testing';

import { NxModule } from '@nrwl/nx';
import { DataPersistence } from '@nrwl/nx';
import { hot } from '@nrwl/nx/testing';

import { PizzaStoreEffects } from './pizza-store.effects';
import { LoadPizzaStore, PizzaStoreLoaded } from './pizza-store.actions';

describe('PizzaStoreEffects', () => {
  let actions: Observable<any>;
  let effects: PizzaStoreEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NxModule.forRoot(),
        StoreModule.forRoot({}),
        EffectsModule.forRoot([])
      ],
      providers: [
        PizzaStoreEffects,
        DataPersistence,
        provideMockActions(() => actions)
      ]
    });

    effects = TestBed.get(PizzaStoreEffects);
  });

  describe('loadPizzaStore$', () => {
    it('should work', () => {
      actions = hot('-a-|', { a: new LoadPizzaStore() });
      expect(effects.loadPizzaStore$).toBeObservable(
        hot('-a-|', { a: new PizzaStoreLoaded([]) })
      );
    });
  });
});
