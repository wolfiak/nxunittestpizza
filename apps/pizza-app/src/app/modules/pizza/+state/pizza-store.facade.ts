import { Injectable } from '@angular/core';

import { select, Store } from '@ngrx/store';

import { PizzaStorePartialState } from './pizza-store.reducer';
import { pizzaStoreQuery } from './pizza-store.selectors';
import { LoadPizzaStore } from './pizza-store.actions';

@Injectable()
export class PizzaStoreFacade {
  loaded$ = this.store.pipe(select(pizzaStoreQuery.getLoaded));
  allPizzaStore$ = this.store.pipe(select(pizzaStoreQuery.getAllPizzaStore));
  selectedPizzaStore$ = this.store.pipe(
    select(pizzaStoreQuery.getSelectedPizzaStore)
  );

  constructor(private store: Store<PizzaStorePartialState>) {}

  loadAll() {
    this.store.dispatch(new LoadPizzaStore());
  }
}
