import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {
  PIZZASTORE_FEATURE_KEY,
  initialState as pizzaStoreInitialState,
  pizzaStoreReducer
} from './+state/pizza-store.reducer';
import { PizzaStoreEffects } from './+state/pizza-store.effects';
import { PizzaStoreFacade } from './+state/pizza-store.facade';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(PIZZASTORE_FEATURE_KEY, pizzaStoreReducer, {
      initialState: pizzaStoreInitialState
    }),
    EffectsModule.forFeature([PizzaStoreEffects])
  ],
  declarations: [],
  providers: [PizzaStoreFacade]
})
export class PizzaModule {}
